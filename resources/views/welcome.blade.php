<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Dashboard Template · Bootstrap</title>
    <!-- Custom styles for this template -->
    <link href="{{ asset('dist/style.css') }}" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Company name</a>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="#">Sign out</a>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link active" href="#">
                                <span data-feather="home"></span>
                                Регистрация <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span data-feather="shopping-cart"></span>
                                Проекты
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div class="container-fluid">

                    {{--{{message}}--}}

                </div>
            </main>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<!-- Bundled File -->
<script src="{{ asset('dist/bundle.js') }}"></script>
<!-- Scripts -->
{{-- <script type="text/javascript" src="{{ asset('/public/js/app.js') }}" defer></script>--}}
<script type="text/javascript" src="{{ mix('/js/app.js') }}" defer></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
